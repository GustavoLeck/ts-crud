import { prisma } from "../../Prisma/prisma";

export class ConsultUserDatabase {
  async execute(idUser: any) {
    try {
      const response = await prisma.user.findMany({
        where: {
          id: idUser,
        },
      });
      if (response.length == 0) {
        throw new Error("Nenhuma conta encontrada");
      }
      return {
        status: true,
        code: 200,
        message: "Consulta realizada com sucesso.",
        data: response[0],
      };
    } catch (error) {
      return { status: false, code: 500, message: error, data: [] };
    }
  }
}
