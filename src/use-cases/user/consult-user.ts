import { User } from "../../interfaces/usuario";
import { ConsultUserDatabase } from "../../infrastructure/database/consult-user-database";

export class ConsultUser {
  async execute(idUser: String) {
    return await new ConsultUserDatabase().execute(idUser);
  }
}
