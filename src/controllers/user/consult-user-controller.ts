import { Request, Response } from "express";
import { UserModel } from "../../models/user/user-model";
import { ConsultUser } from "../../use-cases/user/consult-user";

export class ConsultUserController {
  async handle(req: Request, res: Response) {
    console.log("=> Rotas de consulta de user utilizada");
    const response = await new ConsultUser().execute(req.body.idUser);
    return res.status(response.code).send(response);
  }
}
