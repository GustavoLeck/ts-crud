import { server } from "./server";

server.listen(5500, () => {
  console.clear();
  console.log(`--Server ON--`);
});
