import { testServer } from "../jest.setup";

describe("Get - Test", () => {
  it("Create/Consult/Update/Delete User", async () => {
    const responseCreate = await testServer.post("/api/user").send({
      Nome: "CRM7",
      Sobrenome: "Teste",
      Usuario: "TesteCRM7",
      Senha: "12345678",
    });

    const responseUpdate = await testServer.put("/api/user").send({
      idUser: responseCreate.body.data.id,
      Nome: "CRM7",
      Sobrenome: "Teste2",
      Usuario: "TesteCRM7",
      Senha: "12341234",
    });

    const responseConsult = await testServer.get("/api/user").send({
      idUser: responseCreate.body.data.id,
    });

    const responseDelete = await testServer.delete("/api/user").send({
      idUser: responseConsult.body.data.id,
    });

    expect(responseCreate.body.code).toEqual(200);
    expect(responseConsult.body.code).toEqual(200);
    expect(responseUpdate.body.code).toEqual(200);
    expect(responseDelete.body.code).toEqual(200);
  });
});
